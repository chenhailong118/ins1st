package com.ins1st.modules.sys.role.mapper;

import com.ins1st.modules.sys.role.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author sun
 * @since 2019-05-08
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
