layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/${map["ge"].bizName}/${@com.ins1st.util.ColumnUtil.underline2Camel(map["ge"].tableName,true)}/queryList'
        , cols: [[
            {type: 'radio'},
            <%for(field in map["columns"]){%>
                {field: '${@com.ins1st.util.ColumnUtil.underline2Camel(field.name,true)}', title: '${field.comment}'},
            <%}%>
        ]]
        , page: true
    });

    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                <%for(field in map["columns"]){%>
                    ${@com.ins1st.util.ColumnUtil.underline2Camel(field.name,true)}: $("#${@com.ins1st.util.ColumnUtil.underline2Camel(field.name,true)}").val(),
                <%}%>
            }
        });
    }

    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });

    /**
     * 新增
     */
    $("#add").click(function () {
        Base.open('添加', Base.ctxPath + '/${map["ge"].bizName}/${@com.ins1st.util.ColumnUtil.underline2Camel(map["ge"].tableName,true)}/add', '480', '400');
    });

    /**
     * 编辑
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
                if (data.length == 0) {
                    Base.fail("至少选择一行数据");
                    return false;
                }
                Base.open('修改', Base.ctxPath + '/${map["ge"].bizName}/${@com.ins1st.util.ColumnUtil.underline2Camel(map["ge"].tableName,true)}/edit?id=' + data[0].id, '480', '400');
    });

    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
           Base.fail("至少选择一行数据");
           return false;
        }
        Base.confirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/${map["ge"].bizName}/${@com.ins1st.util.ColumnUtil.underline2Camel(map["ge"].tableName,true)}/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

});